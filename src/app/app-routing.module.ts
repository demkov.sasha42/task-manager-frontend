import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AuthorizationComponent} from "./features/auth/authorization/authorization.component";
import {BoardsComponent} from "./features/dashboard/boards/boards.component";
import {AuthGuard} from "./auth.guard";
import {TasksComponent} from "./features/task/components/tasks/tasks.component";

const routes: Routes = [
  {path: 'login', component: AuthorizationComponent},
  {path: 'boards', component: BoardsComponent, canActivate: [AuthGuard]},
  {path: 'boards/:id', component: TasksComponent, canActivate: [AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
