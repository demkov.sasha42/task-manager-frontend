import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {filter} from "rxjs";

@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.scss']
})
export class AddPostComponent implements OnInit {

  @Input() addName: string;
  @Output() closeAddBoardWindow: EventEmitter<any> = new EventEmitter<any>();
  @Output() addBoard: EventEmitter<any> = new EventEmitter<any>();
  form: FormGroup;

  constructor() { }

  ngOnInit(): void {

  }



}
