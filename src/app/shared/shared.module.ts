import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToolbarComponent } from './toolbar/toolbar.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { AddPostComponent } from './add-post/add-post.component';
import { SearchComponent } from './search/search.component';



@NgModule({
  declarations: [
    ToolbarComponent,
    AddPostComponent,
    SearchComponent
  ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule
    ],
  exports: [
    ToolbarComponent
  ]
})
export class SharedModule { }
