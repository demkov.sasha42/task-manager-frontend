import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  @Input() nameToolbar: string;
  @Input() addBoardWindow: boolean;
  @Input() addName: string;
  @Output() toolbarEmitter: EventEmitter<any> = new EventEmitter<any>();
  @Output() addPost: EventEmitter<any> = new EventEmitter<any>();
  @Output() toggleForm: EventEmitter<any> = new EventEmitter<any>();
  @Output() searchFormEmitter: EventEmitter<any> = new EventEmitter();
  @Output() selectedSortEmitter: EventEmitter<any> = new EventEmitter();
  @Output() selectedOrderEmitter: EventEmitter<any> = new EventEmitter();
  @Input() selectedSort: string;
  @Input() selectedOrder: string;
  searchValue: string;
  formSearch: FormGroup;
  form: FormGroup;


  constructor() { }

  ngOnInit(): void {
    this.formSearch = new FormGroup({
      search: new FormControl,
    })
    this.form = new FormGroup({
      name: new FormControl,
      description: new FormControl
    })
  }



}
