import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {IBoard, INewBoard, INewBoardEdit} from "../features/interfaces/iboard";
import {Observable} from "rxjs";
import {HOST} from "../host/host";

@Injectable({
  providedIn: 'root'
})
export class BoardsService {

  constructor(private http: HttpClient) { }

  getBoards(): Observable<IBoard[]> {
    return this.http.get<IBoard[]>(`${HOST}/api/boards`, {withCredentials: true});
  }

  createNewBoard(name: string, description: string) :Observable<IBoard> {
    return this.http.post<IBoard>(`${HOST}/api/boards`, {name, description}, {withCredentials: true});
  }

  deleteBoardById(boardId: string): Observable<string> {
    return this.http.delete<string>(`${HOST}/api/boards/${boardId}`, {withCredentials: true});
  }

  editBoardById(boardId: string, name: string, description: string): Observable<IBoard> {
    return this.http.patch<IBoard>(`${HOST}/api/boards/${boardId}`,
      {name, description},
      {withCredentials: true})
  }

  editBoardColors(boardId: string, column: string, inputColor: string, ): Observable<IBoard> {
    return this.http.patch<IBoard>(`${HOST}/api/boards/${boardId}/changeColor`,
      {column, inputColor},
      {withCredentials: true})
  }

  createNewTask(boardId: string, name: string, description: string) :Observable<IBoard> {
    return this.http.post<IBoard>(`${HOST}/api/boards/${boardId}`, {name, description}, {withCredentials: true});
  }

  editTaskById(boardId: string, taskId: string, name: string, description: string): Observable<IBoard> {
    return this.http.patch<IBoard>(`${HOST}/api/boards/${boardId}/${taskId}`,
      {name, description},
      {withCredentials: true});
  }

  deleteTaskById(boardId: string, taskId: string): Observable<IBoard> {
    return this.http.delete<IBoard>(`${HOST}/api/boards/${boardId}/${taskId}`, {withCredentials: true});
  }

}
