import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {HOST} from "../host/host"
import {map, Observable} from "rxjs";
import {IUserData} from "../features/interfaces/iuser-data";
import {IUser, IUsername} from "../features/interfaces/iuser";

@Injectable({providedIn: 'root'})
export class AuthService {

  constructor(private http: HttpClient) { }

  registrationUser(username: string, password: string) {
    return this.http.post<IUserData>(`${HOST}/api/auth/register`, {
      username: username,
      password: password
    }, {withCredentials: true})
  }

  loginUser(user: IUserData): Observable<IUser> {
    const {username, password} = user;
    return this.http.post<IUser>(`${HOST}/api/auth/login`, {
      username: username,
      password: password
    }, {withCredentials: true})
  }

  checkAuth() {
    return this.http.get<IUser>(`${HOST}/api/auth/authUser`, {withCredentials: true})
  }

  logout() {
    return this.http.post(`${HOST}/api/auth/logOut`, {}, {withCredentials: true})
  }

  editUsername(value: {username: string}): Observable<{username: string}> {
    return this.http.patch<{username: string}>(`${HOST}/api/user`,
      {username: value.username}, {withCredentials: true})
  }

  deleteUser() {
    return this.http.delete(`${HOST}/api/user`, {withCredentials: true})
  }
}
