import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {AuthorizationModule} from "./features/auth/authorization/authorization.module";
import {CoreModule} from "./core/core.module";
import {HttpClientModule} from "@angular/common/http";
import {StoreModule} from "@ngrx/store";
import {StoreDevtoolsModule} from "@ngrx/store-devtools";
import {environment} from "../environments/environment";
import {userReducer} from "./core/state/user/user.reducer";
import {EffectsModule} from "@ngrx/effects";
import {UserEffects} from "./core/state/user/user.effects";
import {DashboardModule} from "./features/dashboard/dashboard.module";
import {boardsReducer} from "./core/state/boards/boards.reducer";
import {BoardsEffects} from "./core/state/boards/boards.effects";
import { SearchPipe } from './pipes/search.pipe';
import { SortPipe } from './pipes/sort.pipe';
import {TaskModule} from "./features/task/task.module";

@NgModule({
    declarations: [
        AppComponent,
        SearchPipe,
        SortPipe
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        AuthorizationModule,
        CoreModule,
        HttpClientModule,
        StoreModule.forRoot({user: userReducer, boards: boardsReducer}),
        EffectsModule.forRoot([UserEffects, BoardsEffects]),
        StoreDevtoolsModule.instrument({
            maxAge: 25,
            logOnly: environment.production,
            autoPause: true,
        }),
    ],
    providers: [],
  exports: [
    SearchPipe,
    SortPipe
  ],
    bootstrap: [AppComponent]
})
export class AppModule { }
