import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {AuthService} from "./services/auth.service";
import {Store} from "@ngrx/store";
import {appLoadUser, logOutUser, logOutUserSuccess} from "./core/state/user/user.actions";
import {selectUserOther} from "./core/state/user/user.selector";
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [AuthService]
})
export class AppComponent implements OnInit {
  title = 'fullstack-angular';

  constructor(private http: HttpClient,
              private authService: AuthService,
              private store: Store,
              private router: Router) {
  }

  ngOnInit() {
    this.store.dispatch(appLoadUser())
    this.store.select(selectUserOther).subscribe()
  }

  test() {
    let user;
    this.store.select(selectUserOther).subscribe(data => {
      console.log(data)
      user = data;
    })
  }

}
