import { Component, OnInit } from '@angular/core';
import {Store} from "@ngrx/store";
import {selectUserOther} from "../../state/user/user.selector";
import {deleteUser, editUser, logOutUser} from "../../state/user/user.actions";
import {FormControl, FormGroup} from "@angular/forms";
import {IUsername} from "../../../features/interfaces/iuser";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  isAuth: boolean = false;
  userInfo: boolean = false;
  fieldUsername: boolean = false;
  username: string = '';
  image: string = '../../../../assets/man-avatar-character-isolated-icon-free-vector.jpg';

  form: FormGroup;

  constructor(private store: Store) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      username: new FormControl()
    })
    this.store.select(selectUserOther).subscribe(data => {
      if (data.image) {
        this.image = data.image;
      }
      if (data.username) {
        this.username = data.username;
        this.isAuth = true;
      }
    })
  }

  logOut() {
    this.store.dispatch(logOutUser())
    this.userInfo = !this.userInfo;
    this.isAuth = false;
  }

  showUserInfo() {
    this.userInfo = !this.userInfo;
  }

  changeUsername() {
    this.fieldUsername = true;
  }

  async getValueUsername(userData: {value: {username: string}, reset(): void}) {
    await this.store.dispatch(editUser(userData.value))
    this.fieldUsername = false;
    userData.reset();
  }

  deleteUserAccount() {
    this.store.dispatch(deleteUser());
    this.userInfo = false;
    this.isAuth = false;
  }

}
