import {createReducer, on} from "@ngrx/store";
import {initialStateBoards} from "./boards.state";
import {
  addBoardsSuccess, changeBoardColorSuccess,
  createBoardSuccess,
  createTaskSuccess,
  deleteBoardSuccess, deleteTaskSuccess,
  editBoardSuccess, editTaskSuccess
} from "./boards.actions";
import {logOutUserSuccess} from "../user/user.actions";
import {state} from "@angular/animations";


export const boardsReducer = createReducer(
  initialStateBoards,
  on(addBoardsSuccess, (state, boards) => ({boards: boards.boards})),
  on(logOutUserSuccess, () => initialStateBoards),
  on(createBoardSuccess, (state, board) => {
    return {boards: [board, ...state.boards]}
  }),
  on(deleteBoardSuccess, (state, id) => {
    return {boards: [...state.boards.filter(board => `/${board._id}` !== id.id)]}
  }),
  on(editBoardSuccess, (state, responseBoard) => {
    const indexNewBoard = state.boards.findIndex(board => board._id === responseBoard._id);
    const updatedBoards = [...state.boards];
    updatedBoards[indexNewBoard] = responseBoard
    return {...state, boards: updatedBoards}
  }),
  on(createTaskSuccess, (state, responseBoard) => {
    const indexNewBoard = state.boards.findIndex(board => board._id === responseBoard._id);
    const updatedBoards = [...state.boards];
    updatedBoards[indexNewBoard] = responseBoard;
    return {...state, boards: updatedBoards}
  }),
  on(editTaskSuccess, (state, response) => {
    const indexNewBoard = state.boards.findIndex(board => board._id === response._id);
    const updatedBoards = [...state.boards];
    updatedBoards[indexNewBoard] = response
    return {...state, boards: updatedBoards}
  }),
  on(deleteTaskSuccess, (state, response) => {
    const indexNewBoard = state.boards.findIndex(board => board._id === response._id);
    const updatedBoards = [...state.boards];
    updatedBoards[indexNewBoard] = response
    return {...state, boards: updatedBoards}
  }),
  on(changeBoardColorSuccess, (state, response) => {
    const indexNewBoard = state.boards.findIndex(board => board._id === response._id);
    const updatedBoards = [...state.boards];
    updatedBoards[indexNewBoard] = response
    return {...state, boards: updatedBoards}
  })
)
