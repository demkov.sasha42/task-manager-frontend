import {IBoard} from "../../../features/interfaces/iboard";

export interface BoardsState {
  boards: IBoard[];
}

export const initialStateBoards: BoardsState = {
  boards: [],
}
