import {createAction, props} from "@ngrx/store";
import {IBoard, INewBoard, INewBoardEdit} from "../../../features/interfaces/iboard";

export const addLoadBoards = createAction('App load boards');

export const addBoardsSuccess = createAction('Add Boards Success',
  props<{boards: IBoard[]}>()
);

export const addBoardsError = createAction('Add Boards Error',
  props<{error: Error}>()
);

export const createBoard = createAction('Create New Board',
  props<INewBoard>()
);

export const createBoardSuccess = createAction('Create New Board Success',
  props<IBoard>()
);

export const createBoardError = createAction('Create New Board Error',
  props<{error: Error}>()
);

export const deleteBoard = createAction('Delete Board',
  props<{id: string}>()
)

export const deleteBoardSuccess = createAction('Delete Board Success',
  props<{id: string}>()
);

export const deleteBoardError = createAction('Delete Board Error',
  props<{error: Error}>()
)

export const editBoard = createAction('Edit Board',
  props<INewBoardEdit>()
)

export const editBoardSuccess = createAction('Edit Board Success',
  props<IBoard>()
);

export const editBoardError = createAction('Edit Board Error',
  props<{error: Error}>()
)

export const createTask = createAction('Create Task',
  props<INewBoardEdit>()
)

export const createTaskSuccess = createAction('Create New Task Success',
  props<IBoard>()
);

export const createTaskError = createAction('Create New Task Error',
  props<{error: Error}>()
);

export const editTask = createAction('Edit Task',
  props<{boardId: string, taskId: string, name: string, description: string}>()
)

export const editTaskSuccess = createAction('Edit Task Success',
  props<IBoard>()
)

export const EditTaskError = createAction('Edit Task Error',
  props<{error: Error}>()
);

export const deleteTask = createAction('Delete Task',
  props<{boardId: string, taskId: string}>()
);

export const deleteTaskSuccess = createAction('Delete Task Success',
  props<IBoard>()
);

export const deleteTaskError = createAction('Delete Task Error',
  props<{error: Error}>()
);

export const changeBoardColor = createAction('Edit Board Color',
  props<{boardId: string, column: string, inputColor: string}>()
);

export const changeBoardColorSuccess = createAction('Edit Board Color Success',
  props<IBoard>()
);

export const changeBoardColorError = createAction('Edit Board Color Error',
  props<{error: Error}>()
);
