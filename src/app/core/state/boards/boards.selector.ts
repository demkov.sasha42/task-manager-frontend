import {createFeatureSelector, createSelector} from "@ngrx/store";
import {BoardsState} from "./boards.state";

export const selectBoards = createFeatureSelector<BoardsState>('boards');

export const selectBoardsData = createSelector(
  selectBoards,
  (state: BoardsState) => state.boards
)
