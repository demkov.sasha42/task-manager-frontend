import {Injectable} from "@angular/core";
import {BoardsService} from "../../../services/boards.service";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {map, switchMap, catchError, of} from "rxjs";
import {
  addBoardsError,
  addBoardsSuccess,
  addLoadBoards, changeBoardColor, changeBoardColorError, changeBoardColorSuccess,
  createBoard,
  createBoardError,
  createBoardSuccess,
  createTask, createTaskError, createTaskSuccess,
  deleteBoard,
  deleteBoardError,
  deleteBoardSuccess, deleteTask, deleteTaskError, deleteTaskSuccess,
  editBoard,
  editBoardError,
  editBoardSuccess, editTask, EditTaskError, editTaskSuccess
} from "./boards.actions";

@Injectable()
export class BoardsEffects {

  constructor(private boardsService: BoardsService,
              private actions$: Actions) {
  }

  getBoards$ = createEffect(() => this.actions$.pipe(
    ofType(addLoadBoards),
    switchMap(() => this.boardsService.getBoards().pipe(
      map(boards => addBoardsSuccess({boards: boards})),
      catchError(error => of(addBoardsError({error})))
    ))
  ))

  createBoard$ = createEffect(() => this.actions$.pipe(
    ofType(createBoard),
    switchMap((action) => this.boardsService.createNewBoard(action.name, action.description).pipe(
      map((board) => createBoardSuccess(board)),
      catchError(error => of(createBoardError({error})))
    ))
  ))

  deleteBoard$ = createEffect(() => this.actions$.pipe(
    ofType(deleteBoard),
    switchMap((action) => this.boardsService.deleteBoardById(action.id).pipe(
      map((id) => deleteBoardSuccess({id})),
      catchError(error => of(deleteBoardError({error})))
    ))
  ))

  editBoard$ = createEffect(() => this.actions$.pipe(
    ofType(editBoard),
      switchMap((action) => this.boardsService.editBoardById(action.id, action.name, action.description).pipe(
        map((board) => editBoardSuccess(board)),
        catchError(error => of(editBoardError({error})))
      ))
  ))

  createTask$ = createEffect(() => this.actions$.pipe(
    ofType(createTask),
    switchMap((action) => this.boardsService.createNewTask(action.id, action.name, action.description).pipe(
      map((board) => createTaskSuccess(board)),
      catchError(error => of(createTaskError({error})))
    ))
  ))

  editTask$ = createEffect(() => this.actions$.pipe(
    ofType(editTask),
    switchMap(action => this.boardsService.editTaskById(action.boardId, action.taskId, action.name, action.description).pipe(
      map(response => editTaskSuccess(response)),
      catchError(error => of(EditTaskError({error})))
    ))
  ))

  deleteTask$ = createEffect(() => this.actions$.pipe(
    ofType(deleteTask),
    switchMap((action) => this.boardsService.deleteTaskById(action.boardId, action.taskId).pipe(
      map((response) => deleteTaskSuccess(response)),
      catchError(error => of(deleteTaskError({error})))
    ))
  ))

  editBoardColor$ = createEffect(() => this.actions$.pipe(
    ofType(changeBoardColor),
    switchMap(action => this.boardsService.editBoardColors(action.boardId, action.column, action.inputColor).pipe(
      map(response => changeBoardColorSuccess(response)),
      catchError(error => of(changeBoardColorError({error})))
    ))
  ))

}
