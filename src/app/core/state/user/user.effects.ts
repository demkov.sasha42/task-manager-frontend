import {Injectable} from "@angular/core";
import {AuthService} from "../../../services/auth.service";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {
  addUserError,
  addUserSuccess,
  appLoadUser, deleteUser, deleteUserError, deleteUserSuccess, editUser, editUserError, editUserSuccess,
  loginUser,
  loginUserError,
  loginUserSuccess,
  logOutUser, logOutUserError, logOutUserSuccess
} from "./user.actions";
import {map, switchMap, catchError, of, tap} from "rxjs";
import {Router} from "@angular/router";
@Injectable()
export class UserEffects {

  constructor(private authService: AuthService,
              private actions$: Actions,
              private router: Router) {
  }

  getUser$ = createEffect(() => this.actions$.pipe(
    ofType(appLoadUser),
    switchMap(() => this.authService.checkAuth().pipe(
      map(user => addUserSuccess({user})),
      tap(() => this.router.navigate(['/boards'])),
      catchError(error => of(addUserError({error})))
    )),
  ))

  loginUser$ = createEffect(() => this.actions$.pipe(
    ofType(loginUser),
    switchMap((action) => this.authService.loginUser(action).pipe(
      map(user => loginUserSuccess(user)),
      tap(() => this.router.navigate(['/boards'])),
      catchError(error => of(loginUserError({error})))
    )),
  ))

  logOutUser$ = createEffect(() => this.actions$.pipe(
    ofType(logOutUser),
    switchMap(() => this.authService.logout().pipe(
      map(() => logOutUserSuccess()),
      tap(() => this.router.navigate(['/login'])),
      catchError(error => of(logOutUserError({error})))
    )),

  ))

  editUser$ = createEffect(() => this.actions$.pipe(
    ofType(editUser),
    switchMap((action) => this.authService.editUsername(action).pipe(
      map((username) => editUserSuccess({username: username.username})),
      catchError(error => of(editUserError({error})))
    ))
  ))

  deleteUser$ = createEffect(() => this.actions$.pipe(
    ofType(deleteUser),
    switchMap(() => this.authService.deleteUser().pipe(
      map(() => deleteUserSuccess()),
      tap(() => this.router.navigate(['/login'])),
      catchError(error => of(deleteUserError(error)))
    )),
  ))
}
