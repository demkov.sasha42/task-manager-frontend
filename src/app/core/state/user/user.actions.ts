import {IUser, IUsername} from "../../../features/interfaces/iuser";
import {createAction, props} from "@ngrx/store";
import {IUserData} from "../../../features/interfaces/iuser-data";

export const appLoadUser = createAction('App Load User');

export const addUserSuccess = createAction('Add User Success',
  props<{user: IUser}>()
);

export const addUserError = createAction('Add User Error',
  props<{error: Error}>()
);

export const loginUser = createAction('Load User',
  props<IUserData>()
);

export const loginUserSuccess = createAction('Load User Success',
  props<IUser>()
);

export const loginUserError = createAction('Load User Error',
  props<{error: Error}>()
);

export const logOutUser = createAction('Logout User');

export const logOutUserSuccess = createAction('Logout User Success');

export const logOutUserError = createAction('Logout User Error',
  props<{error: Error}>()
);

export const editUser = createAction('Edit User',
  props<{username: string}>()
);

export const editUserSuccess = createAction('Edit User Success',
  props<{username: string}>()
);

export const editUserError = createAction('Edit User Error',
  props<{error: Error}>()
);

export const deleteUser = createAction('Delete User');

export const deleteUserSuccess = createAction('Delete User Success');

export const deleteUserError = createAction('Delete User Error',
  props<{error: Error}>()
);
