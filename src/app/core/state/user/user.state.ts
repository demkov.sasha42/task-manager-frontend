import {IUser} from "../../../features/interfaces/iuser";

export interface UserState {
  user: IUser;
}

export const initialState: UserState = {
  user: {_id: '', username: '', image: ''}
}
