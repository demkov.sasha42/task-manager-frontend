import {createReducer, on} from "@ngrx/store";
import {initialState} from "./user.state";
import {
  addUserSuccess,
  appLoadUser,
  deleteUserSuccess,
  editUserSuccess, loginUser,
  loginUserSuccess,
  logOutUserSuccess
} from "./user.actions";

export const userReducer = createReducer(

  initialState,
  on(appLoadUser, (state) => ({...state})),
  on(addUserSuccess, (state, user) => ({user: {...user.user}})),
  on(loginUser, (state) => ({...state})),
  on(loginUserSuccess, (state, user) => {
    return {user: {...user}}
  }),
  on(logOutUserSuccess, () => initialState),
  on(editUserSuccess, (state, username) => ({...state, user: {...state.user, username: username.username}})
),
  on(deleteUserSuccess, () => initialState)
)
