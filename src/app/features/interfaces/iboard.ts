import {ITask} from "./itask";

export interface IBoard {
  _id: string,
  name: string,
  userId: string,
  description: string,
  backgroundColorNew: string,
  backgroundColorProgress: string,
  backgroundColorDone: string,
  tasks: ITask[],
  createdAt: Date,
  updatedAt: Date
}

export interface INewBoard {
  name: string,
  description: string
}

export interface INewBoardEdit {
  id: string
  name: string,
  description: string,
}
