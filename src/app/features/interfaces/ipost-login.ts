export interface IPostLogin {
  message: string,
  jwt_token: string
}
