export interface ITask {
  _id: string,
  name: string,
  description: string,
  status: string,
  isArchived: boolean,
  image: string,
  borderColor: string,
  createdAt: Date
}
