export interface IUser {
  username: string,
  _id: string,
  image: string
}

export interface IUsername {
  username: string
}
