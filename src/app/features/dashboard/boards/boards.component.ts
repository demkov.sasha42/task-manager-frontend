import {Component, EventEmitter, Input, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";
import {addLoadBoards, createBoard} from "../../../core/state/boards/boards.actions";
import {selectBoardsData} from "../../../core/state/boards/boards.selector";
import {IBoard, INewBoard} from "../../interfaces/iboard";
import {Observable, Subscriber, Subscription} from "rxjs";
import {logOutUser} from "../../../core/state/user/user.actions";

@Component({
  selector: 'app-boards',
  templateUrl: './boards.component.html',
  styleUrls: ['./boards.component.scss']
})
export class BoardsComponent implements OnInit {

  boards: IBoard[];
  subscriber: Subscription;
  addBoardWindow: boolean = false;
  textSearch: string;
  selectedSort: string = 'none';
  selectedOrder: string = 'ascending';


  constructor(private store: Store) { }

  ngOnInit(): void {
    this.store.dispatch(addLoadBoards());
    this.subscriber = this.store.select(selectBoardsData).subscribe(data => {
      this.boards = data;
    })
  }

  toggleFormToolbar():void {
    this.addBoardWindow = !this.addBoardWindow;
  }

  ngOnDestroy():void {
    this.subscriber.unsubscribe();
  }

  async addBoard(boardData: {value: INewBoard, reset(): void}) {
    await this.store.dispatch(createBoard(boardData.value));
    this.addBoardWindow = false;
    boardData.reset();
  }

  checkWhat(input: string):void {
    this.textSearch = input;
  }

  selectSort(event: string): void {
    this.selectedSort = event;
  }

  selectOrder(event: string): void {
    this.selectedOrder = event;
  }
}
