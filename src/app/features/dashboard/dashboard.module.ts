import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BoardsComponent } from './boards/boards.component';
import {BoardModule} from "./board/board.module";
import {SharedModule} from "../../shared/shared.module";
import {AppModule} from "../../app.module";

@NgModule({
  declarations: [
    BoardsComponent,
  ],
    imports: [
        CommonModule,
        BoardModule,
        SharedModule,
        AppModule
    ],
  exports: [
    BoardsComponent
  ]
})
export class DashboardModule { }
