import {Component, Input, OnInit} from '@angular/core';
import {IBoard} from "../../interfaces/iboard";
import {FormControl, FormGroup} from "@angular/forms";
import {Store} from "@ngrx/store";
import {deleteBoard, editBoard} from "../../../core/state/boards/boards.actions";
import {Router} from "@angular/router";

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss']
})
export class BoardComponent implements OnInit {

  @Input() board: IBoard;
  formEdit: FormGroup;
  showEditWindow: boolean = false;

  showOptionsBoard: boolean = false

  constructor(private store: Store,
              private router: Router) { }

  editRemoveBoardButton(): void {
    this.showOptionsBoard = !this.showOptionsBoard;
  }

  editBoard(_id: string, form: {value: {name: string, description: string}, reset(): void}): void {
    this.store.dispatch(editBoard({id: _id, name: form.value.name, description: form.value.description}))
    form.reset();
  }

  deleteBoard(boardId: string) {
    this.store.dispatch(deleteBoard({id: boardId}))
  }

  ngOnInit(): void {
    this.formEdit = new FormGroup({
      name: new FormControl(`${this.board.name}`),
      description: new FormControl(`${this.board.name}`)
    });
  }

  openBoard(boardId: string): void {
    this.router.navigate(['/boards', boardId])
  }
}
