import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BoardComponent} from "./board.component";
import {ReactiveFormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";

@NgModule({
  declarations: [BoardComponent],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        RouterModule
    ],
  exports: [
    BoardComponent
  ]
})
export class BoardModule { }
