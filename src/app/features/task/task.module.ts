import { NgModule } from '@angular/core';
import {CommonModule} from "@angular/common";
import {TasksComponent} from "./components/tasks/tasks.component";
import { TaskComponent } from './components/task/task.component';
import { ColumnTaskComponent } from './components/column-task/column-task.component';
import {SharedModule} from "../../shared/shared.module";
import {AppModule} from "../../app.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    TasksComponent,
    TaskComponent,
    ColumnTaskComponent
  ],
    imports: [
        CommonModule,
        SharedModule,
        AppModule,
        ReactiveFormsModule,
        FormsModule
    ],
  exports: [
    TasksComponent
  ]
})
export class TaskModule {
}
