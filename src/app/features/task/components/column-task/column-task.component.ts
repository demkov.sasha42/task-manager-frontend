import {Component, Input, OnInit} from '@angular/core';
import {ITask} from "../../../interfaces/itask";
import {Store} from "@ngrx/store";
import {ActivatedRoute, Params} from "@angular/router";
import {selectBoardsData} from "../../../../core/state/boards/boards.selector";
import {FormControl, FormGroup} from "@angular/forms";
import {BoardsService} from "../../../../services/boards.service";
import {changeBoardColor} from "../../../../core/state/boards/boards.actions";
@Component({
  selector: 'app-column-task',
  templateUrl: './column-task.component.html',
  styleUrls: ['./column-task.component.scss']
})
export class ColumnTaskComponent implements OnInit {

  tasks: ITask[];
  boardId: string;
  private boardName: string;
  formColor: FormGroup;
  @Input() status: string;
  @Input() textSearch: string;
  @Input() boardTasksBackground: string;
  @Input() selectedSort: string = 'none';
  @Input() selectedOrder: string = 'ascending';

  constructor(private store: Store,
              private route: ActivatedRoute,
              private boardsService: BoardsService) { }

  ngOnInit(): void {

    this.formColor = new FormGroup({
      color: new FormControl(),
      colors_div: new FormGroup({
        color: new FormControl(),
      })
    })

    this.route.params.subscribe((params: Params) => {
      this.boardId = params['id'];
    })

    this.store.select(selectBoardsData).subscribe(boards => {
      boards.find(board => {
        if (board._id === this.boardId) {
          this.boardName = board.name;
          this.tasks = board.tasks;
        }
      })
    })

  }

  changeColor(boardId: string, status: string, formColors: any) {
    if (status === 'NEW') {
      const column: string = 'backgroundColorNew'
      const inputColor: string = formColors.colors_div.color
      this.store.dispatch(changeBoardColor({boardId, column, inputColor}))
    } else if (status === 'IN PROGRESS') {
      const column: string = 'backgroundColorProgress'
      const inputColor: string = formColors.colors_div.color
      this.store.dispatch(changeBoardColor({boardId, column, inputColor}))
    } else if (status === 'DONE'){
      const column: string = 'backgroundColorDone'
      const inputColor: string = formColors.colors_div.color
      this.store.dispatch(changeBoardColor({boardId, column, inputColor}))
    }
  }

}
