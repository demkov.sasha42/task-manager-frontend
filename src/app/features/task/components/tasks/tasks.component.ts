import {Component, Input, OnInit} from '@angular/core';
import {ITask} from "../../../interfaces/itask";
import {Store} from "@ngrx/store";
import {selectBoardsData} from "../../../../core/state/boards/boards.selector";
import {ActivatedRoute, Params} from "@angular/router";
import {INewBoard} from "../../../interfaces/iboard";
import {createBoard, createTask} from "../../../../core/state/boards/boards.actions";

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss']
})
export class TasksComponent implements OnInit {

  tasks: ITask[];
  boardId: string;
  boardName: string;
  selectedSort: string = 'none';
  selectedOrder: string = 'ascending';
  addBoardWindow: boolean = false;
  boardTasksBackgroundNew: string;
  boardTasksBackgroundProgress: string;
  boardTasksBackgroundDone: string;
  @Input() textSearch: string;

  constructor(private store: Store,
              private route: ActivatedRoute
  ) { }

  ngOnInit(): void {

    this.route.params.subscribe((params: Params) => {
      this.boardId = params['id'];
    })

    this.store.select(selectBoardsData).subscribe(boards => {
      boards.find(board => {
        if (board._id === this.boardId) {
          this.boardName = board.name;
          this.tasks = board.tasks;
          this.boardTasksBackgroundNew = board.backgroundColorNew;
          this.boardTasksBackgroundProgress = board.backgroundColorProgress;
          this.boardTasksBackgroundDone = board.backgroundColorDone;
        }
      })
    })

  }

  showInputSearch(input: string) {
    this.textSearch = input;
  }

  selectSort(event: string): void {
    this.selectedSort = event;
  }

  selectOrder(event: string): void {
    this.selectedOrder = event;
  }

  toggleFormToolbar():void {
    this.addBoardWindow = !this.addBoardWindow;
  }

  addBoard(boardId: string, taskData: {value: INewBoard, reset(): void}) {
    this.store.dispatch(createTask({
      id: boardId,
      name: taskData.value.name,
      description: taskData.value.description,
    }))
    this.addBoardWindow = false;
    taskData.reset();
  }

}
