import {Component, Input, OnInit} from '@angular/core';
import {ITask} from "../../../interfaces/itask";
import {Store} from "@ngrx/store";
import {deleteBoard, deleteTask, editTask} from "../../../../core/state/boards/boards.actions";
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent implements OnInit {

  @Input() task: ITask;
  @Input() boardId: string;
  editTaskFormVisible: boolean = false;
  form: FormGroup;

  constructor(private store: Store) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      name: new FormControl(),
      description: new FormControl(),
    })
  }

  deleteTaskBtn(boardId: string, taskId: string) {
    this.store.dispatch(deleteTask({boardId, taskId}))
  }

  submitEditTask(boardId: string, taskId: string, name: string, description: string): void {
    this.store.dispatch(editTask({boardId, taskId, name, description}))

  }

}
