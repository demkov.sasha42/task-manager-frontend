import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SignComponent } from './sign.component';
import {ReactiveFormsModule} from "@angular/forms";



@NgModule({
  declarations: [
    SignComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [
    SignComponent
  ]
})
export class SignModule { }
