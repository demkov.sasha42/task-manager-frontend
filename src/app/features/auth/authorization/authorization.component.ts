import {Component, Input, OnInit} from '@angular/core';
import {AuthService} from "../../../services/auth.service";
import {IUserData} from "../../interfaces/iuser-data";
import {IPostLogin} from "../../interfaces/ipost-login";
import {Store} from "@ngrx/store";
import {loginUser} from "../../../core/state/user/user.actions";

@Component({
  selector: 'app-authorization',
  templateUrl: './authorization.component.html',
  styleUrls: ['./authorization.component.scss'],
  providers: [AuthService]
})
export class AuthorizationComponent implements OnInit {

  @Input() showAuthFormReg: boolean = true;
  authBtnReg: string = 'btn cyan';
  authBtnLog: string = 'btn';

  constructor(private authService: AuthService,
              private store: Store) { }

  ngOnInit(): void {
  }

  switchReg() {
    this.authBtnLog = 'btn';
    this.authBtnReg = 'btn cyan';
    this.showAuthFormReg = true;
  }

  switchLog() {
    this.authBtnReg = 'btn';
    this.authBtnLog = 'btn cyan';
    this.showAuthFormReg = false;
  }

  submitRegister(userData: {value: IUserData, reset(): void}) {
    this.authService.registrationUser(userData.value.username, userData.value.password)
      .subscribe();
    userData.reset();
  }

  submitLogin(userData: {value: IUserData, reset(): void}) {
    this.store.dispatch(loginUser(userData.value))
    userData.reset();
  }
}
