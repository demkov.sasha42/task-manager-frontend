import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthorizationComponent } from './authorization.component';
import {SignModule} from "../sign/sign.module";



@NgModule({
  declarations: [
    AuthorizationComponent
  ],
  imports: [
    CommonModule,
    SignModule
  ],
  exports: [
    AuthorizationComponent
  ]
})
export class AuthorizationModule { }
