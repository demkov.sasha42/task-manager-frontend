import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sort'
})
export class SortPipe implements PipeTransform {

  transform(boards: any, selects: [sort: string, order: string]): any {
    if (selects[1] === 'ascending' && selects[0] === 'name') {
      return boards.sort((a: any, b: any) => {
        if (a.name.toLowerCase() < b.name.toLowerCase()) {
          return -1;
        } else if (a.name.toLowerCase() > b.name.toLowerCase()) {
          return 1;
        }
        return 0;
      });
    } else if (selects[1] === 'descending' && selects[0] === 'name') {
      return boards.sort((a: any, b: any) => {
        if (a.name.toLowerCase() > b.name.toLowerCase()) {
          return -1;
        } else if (a.name.toLowerCase() < b.name.toLowerCase()) {
          return 1;
        }
        return 0;
      });
    } else if (selects[1] === 'ascending' && selects[0] === 'date') {
      return boards.sort((a: any, b: any) => {
        const dateA = new Date(a.createdAt.toString()).getTime();
        const dateB = new Date(b.createdAt.toString()).getTime();
        return dateA - dateB;
        // return (a.createdAt > b.createdAt) ? 1 : ((b.createdAt > a.createdAt) ? -1 : 0)
      })
    } else if (selects[1] === 'descending' && selects[0] === 'date') {
      return boards.sort((a: any, b: any) => {
        const dateA = new Date(a.createdAt.toString()).getTime();
        const dateB = new Date(b.createdAt.toString()).getTime();
        return dateB - dateA;
        // return (a.createdAt < b.createdAt) ? 1 : ((b.createdAt < a.createdAt) ? -1 : 0)
      })
    } else if (selects[0] === 'none') {
      return boards.sort((a: any, b: any) => {
        const dateA = new Date(a.createdAt.toString()).getTime();
        const dateB = new Date(b.createdAt.toString()).getTime();
        return dateA - dateB;
        // return (a.createdAt > b.createdAt) ? 1 : ((b.createdAt > a.createdAt) ? -1 : 0)
      })
    } else {
      return boards;
    }
  }

}
