import { Pipe, PipeTransform } from '@angular/core';
import {IBoard} from "../features/interfaces/iboard";
import {ITask} from "../features/interfaces/itask";

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(array: any, search: string = ''): any {
    if (array.length < 1) {
      return array;
    } else {
      return array.filter((item: any) => item.name.toLowerCase().includes(search.toLowerCase()));
    }
  }
}
